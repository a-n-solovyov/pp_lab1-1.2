﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using System.Threading;
using System.Threading.Tasks;


namespace ConsoleApplication3
{
    internal class Program
    {

        public delegate long TestMethodDelegate(int m, int n);
        
        static readonly int TestCount = 100;
        
        public static void Main(string[] args)
        {
            int[] nArr = {10, 100, 1000, 10000};
            Console.WriteLine(Environment.ProcessorCount);
            Console.WriteLine(Assembly
                .GetEntryAssembly()?
                .GetCustomAttribute<TargetFrameworkAttribute>()?
                .FrameworkName);
            Console.WriteLine("\n_____________________________________");
            Console.WriteLine("Sequential");
            for (int j = 0; j < nArr.Length; j++)
            {
                Console.Write($"{MultipleRunThread(TestCount, TestSequential, 1, nArr[j]), 10}"); 
            }
            Console.WriteLine("\n_____________________________________");
            Console.WriteLine("Parallel");
            for (int j = 0; j < nArr.Length; j++)
            {
                Console.Write($"{MultipleRunThread(TestCount, TestParallel, 1, nArr[j]), 10}"); 
            }
            Console.WriteLine("\n_____________________________________");
            Console.WriteLine("Threads:");
            PrintHeader(nArr);
            PrintResults(nArr, TestThreadA);
            Console.WriteLine("Круговое разделение:");
            PrintResults(nArr, TestThreadB);
            Console.WriteLine("__________________TASKS___________________");
            PrintHeader(nArr);
            PrintResults(nArr, TestTaskA);
            Console.WriteLine("Круговое разделение:");
            PrintResults(nArr, TestTaskB);
            PrintTaskResults(nArr);
        }

        private static void PrintTaskResults(int [] nArr)
        {
           
            int[] mArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            long[,] result = new long[mArr.Length + 1, nArr.Length];

            for (int i = 0; i < mArr.Length; i++)
            {
                for (int j = 0; j < nArr.Length; j++)
                {
                    result[i, j] = MultipleRunThread(TestCount, TestTaskA, mArr[i], nArr[j]);
                }
            }
            
            for (int j = 0; j < nArr.Length; j++)
            {
                result[mArr.Length, j] = MultipleRunThread(TestCount, TestTaskB, 10, nArr[j]);
            }

            Print2DArray(result);
        }

        private static void PrintResults(int[] nArr, TestMethodDelegate test)
        {
            int[] mArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            long[,] result = new long[mArr.Length, nArr.Length];

            for (int i = 0; i < mArr.Length; i++)
            {
                for (int j = 0; j < nArr.Length; j++)
                {
                    result[i, j] = MultipleRunThread(TestCount, test, mArr[i], nArr[j]);
                }
            }
            Print2DArray(result);
        }

        private static long MultipleRunThread(int testCount, TestMethodDelegate @delegate, int m, int n)
        {
            double avg = 0;
            for (int i = 0; i < testCount; i++)
            {
                avg += @delegate(m, n);
            }

            return (int)(avg / testCount);
        }

        private static long TestThreadA(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessThread();
            s.Stop();
            return s.ElapsedTicks;
        }
        
        private static long TestTaskA(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessTask();
            s.Stop();
            return s.ElapsedTicks;
        }

        private static long TestParallel(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessParallel();
            s.Stop();
            return s.ElapsedTicks;
        }
        
        private static long TestSequential(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessSeq();
            s.Stop();
            return s.ElapsedTicks;
        }

        private static long TestThreadB(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessThreadB();
            s.Stop();
            return s.ElapsedTicks;
        }
        
        private static long TestTaskB(int m, int n)
        {
            var t = new Bench(m, n);
            var s = Stopwatch.StartNew();
            t.ProcessTaskB();
            s.Stop();
            return s.ElapsedTicks;
        }

        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write($"{matrix[i,j],10}");
                }
                Console.WriteLine();
            }
        }

        public static void PrintHeader(int[] nArr)
        {
            for (int i = 0; i < nArr.GetLength(0); i++)
            {
                Console.Write($"{nArr[i],10}");
            }
            Console.WriteLine("\n--------------------------------------------");
        }
    }


    public class Bench
    {
        private int _m;
        private int _n;
        private int[] _vector;
        private Func<int, double> _sinFunction = x => Math.Sqrt(x) * Math.Sin(x) / Math.E;
        private List<Thread> _threads;
        private Task[] _tasks;

        public Bench(int m, int n)
        {
            _m = m;
            _n = n;
            _vector = new int[n];
            _threads = new List<Thread>(_m);
            _tasks = new Task[_m];
            FillVector();
        }

        private void FillVector()
        {
            var rnd = new Random();
            for (int i = 0; i < _vector.Length; i++)
                _vector[i] = rnd.Next();
        }
        
        public void ProcessThread()
        {
            
            Action<int, int> processPart = delegate(int start, int end)
            {
                for (int i = start; i < end; i++)
                {
                    _sinFunction(_vector[i]);
                }
            };
            
            for (int i = 0, offset = 0, k = _n/_m; i < _m; i++)
            {
                var offset1 = offset;
                _threads.Add(new Thread(() => processPart(offset1, offset1+k)));
                offset += k;
            }

            for (var i = 0; i < _threads.Count; i++)
            {
                _threads[i].Name = (i+1).ToString(); 
                _threads[i].Start();
            }

            foreach (var x in _threads) x.Join();
            
        }
        
        public void ProcessTask()
        {
            
            Action<int, int> processPart = delegate(int start, int end)
            {
                for (int i = start; i < end; i++)
                {
                    _sinFunction(_vector[i]);
                }
            };
            
            for (int i = 0, offset = 0, k = _n/_m; i < _m; i++)
            {
                var offset1 = offset;
                _tasks[i] = new Task(() => processPart(offset1, offset1+k));
                offset += k;
            }

            foreach (var t in _tasks) t.Start();

            Task.WaitAll(_tasks);
        }

        public void ProcessParallel()
        {
            Parallel.For(0, _vector.Length, //new ParallelOptions(){MaxDegreeOfParallelism = _m},
                                            i =>
            {
                _sinFunction(_vector[i]);
            });
        }

        
        
        public void ProcessThreadB()
        {
            Action<int> processPart = delegate(int start)
            {
                for (int i = start; i < _vector.Length; i += _m)
                    _sinFunction(_vector[i]);
            };
            
            for (int i = 0; i < _m; i++)
            {
                _threads.Add(
                    new Thread(() => processPart(i))
                );
            }

            for (var i = 0; i < _threads.Count; i++)
            {
                _threads[i].Name = (i+1).ToString(); 
                _threads[i].Start();
            }

            foreach (var x in _threads) x.Join();
        }
        
        public void ProcessTaskB()
        {
            Action<int> processPart = delegate(int start)
            {
                for (int i = start; i < _vector.Length; i += _m)
                    _sinFunction(_vector[i]);
            };
            
            for (int i = 0; i < _tasks.Length; i++)
            {
                _tasks[i] = new Task(() => processPart(i));
            }

            foreach (var t in _tasks) t.Start();

            Task.WaitAll(_tasks);
        }

        public void ProcessSeq()
        {
            for (int i = 0; i < _vector.Length; i++)
            {
                _sinFunction(_vector[i]);
            }
        }
    }
}